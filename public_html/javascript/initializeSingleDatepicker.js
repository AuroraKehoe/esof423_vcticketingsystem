$("#datepicker").datepicker({
    inline: true,
    dateFormat: 'yy-mm-dd'
});
//From StackOverflow: stackoverflow.com/questions/32540044/html-display-current-date
n = new Date();
y = n.getFullYear();
m = n.getMonth() + 1;
d = n.getDate();
document.getElementById("datepicker").value = y + "-" + m + "-" + d;