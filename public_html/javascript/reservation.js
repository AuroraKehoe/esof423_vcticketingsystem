var newwindow;

// Open Add Reservation window
function openAddReservation(url) {
    newwindow = window.open(url, 'name', 'height=400,width=775px');
    if (window.focus) {
        newwindow.focus()
    }
}

/* Code for this function modified from https://stackoverflow.com/questions/386281/how-to-implement-select-all-check-box-in-html */

// Select all reservations checkbox
function selectAllReservations(source) {
    checkboxes = document.getElementsByName('reservationCheckbox');

    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = source.checked;
    }
}

// Designate reservation as a Walk In
function makeWalkIn() {

    // Makes a walk in reservation reversible by re-clicking the "Walk In Reservation" checkbox
    $('#makeWalkInReservation').change(function() {
        if(this.checked)
        {
            document.getElementById('personalInfo').style.display = "none";
            document.getElementById('reservationSource').value="Walk In";      
        }
        else{
            document.getElementById('personalInfo').style.display = "block";
            document.getElementById('reservationSource').value=" ";
        }
    });
}