<?php
session_start();
$host = "mysql-server"; /* Host name */
$user = "root"; /* User */
$password = "root"; /* Password */
$dbname = "tickets"; /* Database name */

$con = mysqli_connect($host, $user, $password, $dbname);
// Check connection
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}

/* structure based on code from this website: https://makitweb.com/login-page-with-jquery-and-ajax/ */
