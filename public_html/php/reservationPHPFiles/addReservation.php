<?php
include "../config.php";
$globalSeatArray = [];
include "../seatsPHPFiles/emptySeatArray.php";
//grabbing ticket seat data
$aTix = $_POST['adultTickets'];
$dTix = $_POST['discountTickets'];
$chTix = $_POST['childTickets'];
$coTix = $_POST['compTickets'];
//grabbing notes data
$notes = mysqli_real_escape_string($con,$_POST['notes']);
$seatRequest = mysqli_real_escape_string($con,$_POST['seatRequest']);
//grabbing seats
$seats = mysqli_real_escape_string($con,$_POST['seats']);
//grabbing customer and day/time information
$day = mysqli_real_escape_string($con,$_POST['day']);
$time = mysqli_real_escape_string($con,$_POST['time']);
$firstName = mysqli_real_escape_string($con,$_POST['firstName']);
$lastName = mysqli_real_escape_string($con,$_POST['lastName']);
$phone = mysqli_real_escape_string($con,$_POST['phone']);
$email = mysqli_real_escape_string($con,$_POST['email']);
$location = mysqli_real_escape_string($con,$_POST['location']);
$source = mysqli_real_escape_string($con,$_POST['reservationSource']);
//checking total tickets for easier condition check
$totalTix = $aTix + $dTix + $chTix + $coTix;

$legalSeats = true;

//SEAT VALIDATION
//Create seats array
$seat_query = "select seats as seat from reservations where Day='".$day."' and Time='".$time."'";
$seatsPerReservation = mysqli_query($con,$seat_query);

while ($seatGroup = mysqli_fetch_assoc($seatsPerReservation)) {
    $seatSQL = $seatGroup['seat'];
    if(strlen($seatSQL) > 4){
        $separator = substr($seatSQL,4,2);
        if(strcmp($separator, ", ") !== 0){
            $seatGroupArray = explode(",",$seatGroup['seat']);
        }else{
            $seatGroupArray = explode(", ",$seatGroup['seat']);
        }
    }else{
        $seatGroupArray = [$seatSQL];//only one seat
    }
    foreach($seatGroupArray as $singleSeatGroup){
        if(strlen($singleSeatGroup) < 4){//one seat
            if(strlen($singleSeatGroup) == 2){
                $seatNums = intval(substr($singleSeatGroup,2,1));
            }else{
                $seatNums = intval(substr($singleSeatGroup,2,2));
            }
            array_push($globalSeatArray[substr($singleSeatGroup,0,1)],$seatNums);
        }else{//multiple seats
            if(strlen($singleSeatGroup) == 4){//B1-2
                $startNum = intval(substr($singleSeatGroup,1,1));
                $endNum = intval(substr($singleSeatGroup,-1));
            }else if(strlen($singleSeatGroup) == 5){//B9-10
                $startNum = intval(substr($singleSeatGroup,1,1));
                $endNum = intval(substr($singleSeatGroup,-2));
            }else{//B10-12
                $startNum = intval(substr($singleSeatGroup,1,2));
                $endNum = intval(substr($singleSeatGroup,-2));
            }
            for($i = $startNum; $i < $endNum + 1; $i++){
                array_push($globalSeatArray[substr($singleSeatGroup,0,1)], $i);
            }
        }
    }
}

if(strlen($seats) > 4) {//actual seats from reservation form, multiple seats
    $separator = substr($seats, 4, 2);
    if (strcmp($separator, ", ") !== 0) {
        $seatArr = explode(",", $seats);
    } else {
        $seatArr = explode(", ", $seats);
    }
}else{//only one seat
    $seatArr = [$seats];
}

foreach($seatArr as $seatSection){
    $rowLetter = substr($seatSection,0,1);
    if(strlen($seatSection) < 4){//one seat
        if(strlen($seatSection) < 3){//B1
            $num = intval(substr($seatSection,1,1));
        }else{//B11
            $num = intval(substr($seatSection,1,2));
        }
        if(in_array($num,$globalSeatArray[$rowLetter])){
            $legalSeats = false;
            break;
        }
    }else{//multiple seats
        if(strlen($seatSection) == 4){//B1-2
            $startNum = intval(substr($seatSection,1,1));
            $endNum = intval(substr($seatSection,-1));
        }else if(strlen($seatSection) == 5){//B9-10
            $startNum = intval(substr($seatSection,1,1));
            $endNum = intval(substr($seatSection,-2));
        }else{//B10-12
            $startNum = intval(substr($seatSection,1,2));
            $endNum = intval(substr($seatSection,-2));
        }

        for($i = $startNum; $i < $endNum + 1; $i++){
            if(!empty($globalSeatArray[$rowLetter])) {
                if (in_array($i, $globalSeatArray[$rowLetter])) {
                    $legalSeats = false;
                    break;
                }
            }
        }
    }
}
$correct = 0;
$happened = false;
//SEAT VALIDATION END
if ($totalTix > 0 && !empty($seats) && !empty($day) && !empty($time)){
    if($legalSeats){
        $sql_query = "select customer_id as id,showsSeen as shows from customers where First_Name='".$firstName."' and Last_Name='".$lastName."' and phone='".$phone."';";
        $result = mysqli_query($con,$sql_query);//grabbing customer id to check if they exist
        $row = mysqli_fetch_array($result);

        if(empty($row['id'])){//if this is a new customer
            $in_query = "insert into customers(First_Name,Last_Name,phone,email,Location,ShowsSeen)  values('".$firstName."','".$lastName."','".$phone."','".$email."','".$location."', 1)";
            if(mysqli_query($con,$in_query)){
                $correct = 2;
            }else{//if not all info is there
                echo "Error adding customer: " . $con->error . "<br>";
            }
            $res = mysqli_query($con,$sql_query);
            $row = mysqli_fetch_array($res);
        }else{ //if it's not a new customer, update the number of shows they've seen
            $up_query = "update customers set ShowsSeen=".($row['shows']+1)." where customer_id=".$row['id'].";";
            if(mysqli_query($con,$up_query)){
                $correct = 1;
            }else{
                echo "Error updating: " . $con->error . "<br>";
            }
        }
        //create seats array end
        if(!empty($row['id'])){//if we have a customer to put in
            $reserveQuery = "insert into reservations(customer_id,Day,Time,Adult_Tickets,Discount_Tickets,Child_Tickets,Comp_Tickets,Notes,Seat_Request,Seats, Source) values(" . $row['id'] . ",'" . $day . "','" . $time . "'," . $aTix . "," . $dTix . "," . $chTix . "," . $coTix . ",'" . $notes . "','" . $seatRequest . "','" . $seats . "','" . $source . "')";

            if (mysqli_query($con, $reserveQuery)) {
                $happened = true;
            } else {
                echo "Error adding reservation: " . $con->error;
            }//errors
        }else{//if not
            echo "<br> Can't add reservation, customer not found.";
        }
        if($happened){
            echo $correct;
        }
    }else{
        echo "These seats are reserved already.";
    }
}else{
    echo "Required Fields Missing: Tickets, Seats, Day, or Time.";
}