<?php
include '../config.php';
//same variables as addReservation.php
$id = $_POST['id'];

$aTix = $_POST['adultTickets'];
$dTix = $_POST['discountTickets'];
$chTix = $_POST['childTickets'];
$coTix = $_POST['compTickets'];

$notes = mysqli_real_escape_string($con,$_POST['notes']);
$seatRequest = mysqli_real_escape_string($con,$_POST['seatRequest']);

$seats = mysqli_real_escape_string($con,$_POST['seats']);
$firstName = mysqli_real_escape_string($con,$_POST['firstName']);
$lastName = mysqli_real_escape_string($con,$_POST['lastName']);
$phone = mysqli_real_escape_string($con,$_POST['phone']);
$time = mysqli_real_escape_string($con,$_POST['time']);
//grabbing customer and phone number, things that would change in customer information
$sql_query = "select customer_id as id,phone as phone from customers where First_Name='".$firstName."' and Last_Name='".$lastName."'";
$result = mysqli_query($con,$sql_query);
$row = mysqli_fetch_array($result);

if(empty( $row['id'] )){//if customer doesn't exist, new customer info
    echo "adding customer... <br>";
    $in_query = "insert into customers(First_Name,Last_Name,phone,email,Location,ShowsSeen)  values('".$firstName."','".$lastName."','".$phone."',' ',' ', 1)";
    if(mysqli_query($con,$in_query)){
        echo "New customer added! <br>";
    }else{
        echo "Error adding customer: " . $con->error . "<br>";
    }
    $res = mysqli_query($con,$sql_query);
    $row = mysqli_fetch_array($res);
}else if(strcmp($row['phone'],$phone) != 0){//update customer info
    $up_query = "update customers set phone='".$phone."' where customer_id=".$row['id'].";";
    if(mysqli_query($con,$up_query)){
        echo "Customer phone updated.";
    }else{
        echo "Error updating phone: " . $con->error ;
    }
}
//update reservation query
$update_query = "update reservations set Adult_Tickets = ".$aTix.", Discount_Tickets = ".$dTix.",";
$update_query = $update_query."Time = '".$time."',";
$update_query = $update_query."Child_Tickets = ".$chTix.",";
$update_query = $update_query."Comp_Tickets = ".$coTix.",";
$update_query = $update_query."Notes = '".$notes."',";
$update_query = $update_query."Seat_Request = '".$seatRequest."',";
$update_query = $update_query."Seats = '".$seats."'";
$update_query = $update_query."where reservation_id=".$id.";";

if (mysqli_query($con,$update_query)) {//response checking
    echo "Reservation updated successfully!";
} else {
    echo "Error updating reservation: " . $con->error;
}