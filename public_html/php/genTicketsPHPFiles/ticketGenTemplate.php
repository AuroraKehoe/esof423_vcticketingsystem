<?php

include "../config.php";

$seat_array = [];
//grab variables
$seat_string = $_POST['seats'];
$aTix = $_POST['adultTickets'];
$dTix = $_POST['discountTickets'];
$chTix = $_POST['childTickets'];
$coTix = $_POST['compTickets'];
$resId = $_POST['res_id'];
$name = $_POST['name'];

$ticketHeader = 60;
$seatsHeader = 167;

$today = mysqli_real_escape_string($con, $_POST['date']);
$todayDate = date('Y-m-d',strtotime($today));
$show = "";
//query to get shows, for accurate show in tickets
$show_query = "select show_id as sho, ShowName as name, Start_Date as start, End_Date as end from shows";
$result = mysqli_query($con, $show_query);

while ($shows = mysqli_fetch_assoc($result)) { //while loop to find show id with closest end date to current day

    $showEnd = date('Y-m-d',strtotime($shows['end']));//grab ending date
    $showStart = date('Y-m-d',strtotime($shows['start']));//grab starting date

    if($showStart <= $todayDate && $todayDate <= $showEnd){
        $found = true;
        $show = $shows['name'];
        if($showStart == $showEnd){
            break;
        }
    }
}
//split seats
$aisleSeats = explode(",", $seat_string);

//$rows = array_keys($seat_array);
//
//foreach ($aisleSeats as $seat) {
//    $seat = trim($seat);
//    $seatNums = [];//add to array
//
//    if (strlen($seat) < 4) {
//        if(strlen($seat) == 2){
//            $seatNums = intval(substr($seat, 2, 1));
//        }else{
//            $seatNums = intval(substr($seat, 2, 2));
//        }
//    } else {
//        if(strlen($seat) == 4){//B1-2
//            $startNum = intval(substr($seat,1,1));
//            $endNum = intval(substr($seat,-1));
//        }else if(strlen($seat) == 5){//B9-10
//            $startNum = intval(substr($seat,1,1));
//            $endNum = intval(substr($seat,-2));
//        }else{//B10-12
//            $startNum = intval(substr($seat,1,2));
//            $endNum = intval(substr($seat,-2));
//        }
//        for ($i = $startNum; $i < $endNum + 1; $i++) {
//            array_push($seatNums, $i);
//        }
//    }
//    $seat_array[substr($seat, 0, 1)] = $seatNums; //one seat or many, add the seats to the row key
//}
///* Completed array would look like this!
// * $seat_array= [
// *  "B" => [1,2,3],
// *  "C" => [1,2]
// *  ];
// */
////based on reservation, the number of seats and types will match, don't need to worry about it here
//$rows = array_keys($seat_array);
//
//foreach ($rows as $row) {
//    $seats = $seat_array[$row];
//    foreach ($seats as $seat) {
//
//        //setup echoes
//        echo "<svg width='816' height='192' class=${resId}>";
//        echo "<rect width='816' height='192' stroke='white' stroke-width='4' fill='white' />";
//        echo "<line x1='612' y1='0' x2='612' y2='192' stroke='black' />";
//        echo "<text x='42.5' y='171'>Date:</text>";
//        echo "<line x1='92' y1='171' x2='262' y2='171' stroke='black' />";
//        echo "<text x='283' y='171'>Row:</text>";
//        echo "<line x1='333' y1='171' x2='432' y2='171' stroke='black' />";
//        echo "<text x='453' y='171'>Seat:</text>";
//        echo "<line x1='503' y1='171' x2='581' y2='171' stroke='black' />";
//        echo "<text x='659' y='28'> Admit One </text>";
//
//        //input echoes
//        echo "<text x='99' y='167' class='svgForm' id='date'>${today}</text>";
//        echo "<text x= '371' y='167' class='svgForm' id='row'>${row}</text>";
//        echo "<text x='528' y='167' class='svgForm' id='seat'>${seat}</text>";
//        $tickType = "";
//        if ($aTix > 0) {
//            $tickType = "Adult";
//            $aTix--;
//        } elseif ($dTix > 0) {
//            $tickType = "Discount";
//            $dTix--;
//        } elseif ($chTix > 0) {
//            $tickType = "Child";
//            $chTix--;
//        } elseif ($coTix > 0) {
//            $tickType = "Complementary";
//            $coTix--;
//        } else {
//            $tickType = "ERROR";
//        }
//        echo "<text x='682' y='94' class='svgForm' id='type'>" . $tickType . "</text>";
//
//        if (strcmp($show, "") !== 0) {//if show was valid
//            echo "<image xlink:href='../images/${show}.png' height='98' width='300' x='128' y='20'/>";
//        } else {//if show was empty
//            echo "<ellipse cx='220' cy='50' rx='150' ry='40' fill='white' stroke='black' stroke-width='1'/>";
//            echo "<text x='170' y='55' >Show not found! </text>";
//        }
//
//        echo "</svg>";
//    }
//
//}

//test code

echo "<svg width='816' height='192' class=${resId}>";
echo "<rect width='816' height='192' stroke='white' stroke-width='4' fill='white' />";
echo "<line x1='612' y1='0' x2='612' y2='192' stroke='black' />";
echo "<text x='42.5' y='171'>Date:</text>";
echo "<line x1='92' y1='171' x2='180' y2='171' stroke='black' />";
echo "<text x='192' y='171'>Name:</text>";
echo "<line x1='242' y1='171' x2='442' y2='171' stroke='black' />";
echo "<text x='453' y='171'>Seats:</text>";
echo "<line x1='496' y1='171' x2='604' y2='171' stroke='black' />";
echo "<text x='659' y='28'> Ticket Summary </text>";

echo "<text x='99' y='167' class='svgForm' id='date'>${today}</text>";
echo "<text x= '243' y='167' class='svgForm' id='name'>${name}</text>";
//for loop for seats
$seatsLeft = count($aisleSeats);
$i = 0;
while($seatsLeft > 0){
    if($seatsLeft == 1){
        $line = $aisleSeats[$i];
        echo "<text x='530' y='${seatsHeader}' class='svgForm'>${line}</text>";
    }else{
        $line = $aisleSeats[$i].", ".$aisleSeats[$i+1];
        echo "<text x='497' y='${seatsHeader}' class='svgForm'>${line}</text>";
    }
    $seatsHeader -= 20;
    $seatsLeft -= 2;
    $i += 2;
}
//for($i = 0; $i < count($aisleSeats); $i += 2){
//    $line = $aisleSeats[$i].", ".$aisleSeats[$i+1];
//    echo "<text x='497' y='${seatsHeader}' class='svgForm'>${line}</text>";
//    $seatsHeader -= 20;
//}
if($aTix > 0){
    echo "<text x='640' y='${ticketHeader}' class='svgForm' id='adult'>Adult: </text>";
    echo "<text x='760' y='${ticketHeader}' class='svgForm' id='adultTix'>${aTix}</text>";
    //echo ticket number
    $ticketHeader += 20;
}
if($dTix > 0){
    echo "<text x='640' y='${ticketHeader}' class='svgForm' id='discount'>Discount: </text>";
    echo "<text x='760' y='${ticketHeader}' class='svgForm' id='discountTix'>${dTix}</text>";
    $ticketHeader += 20;
}
if($chTix > 0){
    echo "<text x='640' y='${ticketHeader}' class='svgForm' id='child'>Child: </text>";
    echo "<text x='760' y='${ticketHeader}' class='svgForm' id='childTix'>${chTix}</text>";
    $ticketHeader += 20;
}
if($coTix > 0){
    echo "<text x='640' y='${ticketHeader}' class='svgForm' id='comp'>Complementary: </text>";
    echo "<text x='760' y='${ticketHeader}' class='svgForm' id='compTix'>${coTix}</text>";
    $ticketHeader += 20;
}

if (strcmp($show, "") !== 0) {//if show was valid
    echo "<image xlink:href='../images/${show}.png' height='98' width='300' x='128' y='20'/>";
} else {//if show was empty
    echo "<ellipse cx='220' cy='50' rx='150' ry='40' fill='white' stroke='black' stroke-width='1'/>";
    echo "<text x='170' y='55' >Show not found! </text>";
}

echo "<text x='660' y='170' class='svgForm' id='adult'>Enjoy the Show!</text>";
echo "</svg>";