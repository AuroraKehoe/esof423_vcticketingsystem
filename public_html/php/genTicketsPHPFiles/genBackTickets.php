<?php
include '../config.php';
//grab post data
$today = mysqli_real_escape_string($con, $_POST['date']);
$numTickets = $_POST['totalTickets'];
$resId = $_POST['resId'];

//create date
$todayDate = date('Y-m-d',strtotime($today));

$showEndDate = "";
$showStartDate = "";

//saved sponsor name for png
$currentSponsor = "";

$show_query = "select show_id as sho, ShowName as name, Start_Date as start, End_Date as end from shows";
$result = mysqli_query($con, $show_query);

//while loop to determine what show is running
while ($shows = mysqli_fetch_assoc($result)) { //while loop to find show id with closest end date to current day

    $showEnd = date('Y-m-d',strtotime($shows['end']));//grab ending date
    $showStart = date('Y-m-d',strtotime($shows['start']));//grab starting date

    if($showStart <= $todayDate && $todayDate <= $showEnd){
        $showEndDate = $showEnd;
        $showStartDate = $showStart;
        if($showStart == $showEnd){
            break;
        }
    }
}
//grab sponsor
$sponsor_query = "select * from sponsors";
$result = mysqli_query($con, $sponsor_query);
//while loop to determine which sponsor is running over this show
while ($sponsors = mysqli_fetch_assoc($result)) { //while loop to find show id with closest end date to current day

    $sponsorEnd = date('Y-m-d',strtotime($sponsors['End_Date']));//grab ending date
    $sponsorStart = date('Y-m-d',strtotime($sponsors['Start_Date']));//grab starting date

    if($sponsorEnd >= $showEndDate && $sponsorStart <= $showStartDate){
        $currentSponsor = $sponsors['Sponsor_Name'];
    }
}

//generating corresponding number of backsides for each ticket
for($i = 0; $i < $numTickets; $i++){
//SVG generation
    echo "<svg width='816' height='192' class='${resId} back' >";
    echo "<rect width='816' height='192' stroke='white' stroke-width='4' fill='white' />";
    echo "<line x1='612' y1='0' x2='612' y2='192' stroke='black' />";
    if(strcmp($currentSponsor, "") !== 0){
        echo "<image xlink:href='../images/${currentSponsor}.png' height='98' width='300' x='128' y='20'/>";
    }else{
        echo "<ellipse cx='220' cy='70' rx='200' ry='60' fill='white' stroke='black' stroke-width='1'/>";
        echo "<text x='170' y='55' >Sponsor not found! </text>";
    }
    echo "</svg>";
}
