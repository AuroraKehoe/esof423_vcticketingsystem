<?php
include '../config.php';

$sponsor_query = "SELECT * from sponsors";
$year = $_POST["year"]; // Get the value of the year the user sent in

$sponsor_result = mysqli_query($con, $sponsor_query); // Grab all sponsors from database
while($row = mysqli_fetch_assoc($sponsor_result))
{
    // Hold value of the start year
    $startYear = intval(explode("-", $row["Start_Date"])[0]);
    
    // Display the sponsors that match the year given
    if($year == $startYear)
    {
        echo "<tr>";
        echo "<td id='sponsorName".$row['sponsor_id']."' class='SponsorName'> ".$row["Sponsor_Name"]."</td>";
        echo "<td id='sponsorStartDate".$row['sponsor_id']."' class='sponsorStartDate'> ".$row["Start_Date"]."</td>";
        echo "<td id='sponsorEndDate".$row['sponsor_id']."' class='sponsorEndDate'> ".$row["End_Date"]."</td>";
        echo "<td id='sponsorEditButton".$row['sponsor_id']."'> <button class='edit' id='" . $row['sponsor_id'] . "' onclick='editSponsor(this.id)'>Edit</button></td>";
        echo "<td> <button class='delete' id='" . $row['sponsor_id'] . "' onclick='deleteSponsor(this.id)'>Delete</button></td>";
        echo"</tr>";
    }
}