-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql-server
-- Generation Time: Mar 31, 2021 at 04:51 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tickets`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` bigint NOT NULL,
  `First_Name` varchar(50) DEFAULT NULL,
  `Last_Name` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `Location` varchar(100) DEFAULT NULL,
  `ShowsSeen` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `First_Name`, `Last_Name`, `phone`, `email`, `Location`, `ShowsSeen`) VALUES
(1, 'Arnold', 'Smithson', '406-660-1862', 'arnoldsmithson@outlook.com', ' ', 5),
(7, 'Aurora', 'Kehoe', '406-223-6720', 'x', '', 1),
(8, 'Walk', 'In', 'walkinticket', 'x', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deleted_reservations`
--

CREATE TABLE `deleted_reservations` (
  `deleted_id` bigint NOT NULL,
  `customer_id` bigint DEFAULT NULL,
  `Day` date DEFAULT NULL,
  `Time` varchar(10) DEFAULT NULL,
  `Adult_Tickets` int DEFAULT NULL,
  `Discount_Tickets` int DEFAULT NULL,
  `Child_Tickets` int DEFAULT NULL,
  `Comp_Tickets` int DEFAULT NULL,
  `Notes` varchar(250) DEFAULT NULL,
  `Seat_Request` varchar(250) DEFAULT NULL,
  `Seats` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `reservation_id` bigint NOT NULL,
  `customer_id` bigint DEFAULT NULL,
  `Day` date DEFAULT NULL,
  `Time` varchar(10) DEFAULT NULL,
  `Adult_Tickets` int DEFAULT NULL,
  `Discount_Tickets` int DEFAULT NULL,
  `Child_Tickets` int DEFAULT NULL,
  `Comp_Tickets` int DEFAULT NULL,
  `Notes` varchar(250) DEFAULT NULL,
  `Seat_Request` varchar(250) DEFAULT NULL,
  `Seats` varchar(50) DEFAULT NULL,
  `Source` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Checked_in` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`reservation_id`, `customer_id`, `Day`, `Time`, `Adult_Tickets`, `Discount_Tickets`, `Child_Tickets`, `Comp_Tickets`, `Notes`, `Seat_Request`, `Seats`, `Source`, `Checked_in`) VALUES
(3, 1, '2021-05-27', '4:00', 2, 2, 0, 0, 'Hard of hearing', 'Aisle Seat', 'B1-2, C1-2', '', 0),
(4, 7, '2021-05-27', '4:00', 3, 0, 0, 0, 'Hard of hearing', 'Close to front', 'A1-3', '', 0),
(5, 8, '2021-03-30', '4:00', 3, 0, 0, 0, 'pls walk in', 'x', 'A1-3', 'Walk In', 0);

-- --------------------------------------------------------

--
-- Table structure for table `season_passes`
--

CREATE TABLE `season_passes` (
  `pass_id` bigint NOT NULL,
  `customer_id` bigint DEFAULT NULL,
  `PassType` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shows`
--

CREATE TABLE `shows` (
  `show_id` bigint NOT NULL,
  `sponsor_id` bigint DEFAULT NULL,
  `ShowName` varchar(150) NOT NULL,
  `Start_Date` date DEFAULT NULL,
  `End_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shows`
--

INSERT INTO `shows` (`show_id`, `sponsor_id`, `ShowName`, `Start_Date`, `End_Date`) VALUES
(3, NULL, 'Robin Hood', '2021-05-22', '2021-06-17'),
(4, NULL, 'Pirates of Penzance', '2021-04-23', '2021-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `sponsor_id` bigint NOT NULL,
  `Sponsor_Name` varchar(150) DEFAULT NULL,
  `Start_Date` date DEFAULT NULL,
  `End_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`sponsor_id`, `Sponsor_Name`, `Start_Date`, `End_Date`) VALUES
(1, 'Rich Sponsor Yay', '2021-04-23', '2021-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(80) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `password`) VALUES
(1, 'bill', 'admin', 'password'),
(3, 'dev', 'admin', 'password');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `deleted_reservations`
--
ALTER TABLE `deleted_reservations`
  ADD PRIMARY KEY (`deleted_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`reservation_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `season_passes`
--
ALTER TABLE `season_passes`
  ADD PRIMARY KEY (`pass_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `shows`
--
ALTER TABLE `shows`
  ADD PRIMARY KEY (`show_id`),
  ADD KEY `sponsor_id` (`sponsor_id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`sponsor_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `deleted_reservations`
--
ALTER TABLE `deleted_reservations`
  MODIFY `deleted_id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `reservation_id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `season_passes`
--
ALTER TABLE `season_passes`
  MODIFY `pass_id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shows`
--
ALTER TABLE `shows`
  MODIFY `show_id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `sponsor_id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deleted_reservations`
--
ALTER TABLE `deleted_reservations`
  ADD CONSTRAINT `deleted_reservations_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`);

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`);

--
-- Constraints for table `season_passes`
--
ALTER TABLE `season_passes`
  ADD CONSTRAINT `season_passes_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`);

--
-- Constraints for table `shows`
--
ALTER TABLE `shows`
  ADD CONSTRAINT `shows_ibfk_1` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsors` (`sponsor_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
